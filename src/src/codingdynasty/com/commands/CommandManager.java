package src.codingdynasty.com.commands;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import src.codingdynasty.com.utils.MessageManager;

public class CommandManager implements CommandExecutor{
	
	private ArrayList<SubCommand> commands = new ArrayList<SubCommand>();

	public void setup() {
		commands.add(new SetLevel());
		commands.add(new setSpawn());
		commands.add(new inSpawn());
		commands.add(new Level());
		commands.add(new ModMode());
		commands.add(new Boss());
		commands.add(new Butcher());
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label,
			String[] args) {
		
		
		if (!(sender instanceof Player)) {
			System.out.println(ChatColor.RED + "Console cannot run command!");
			return true;
		}
		
		Player p = (Player) sender;
		
		if (command.getName().equalsIgnoreCase("lunarMobs")) {
			if (args.length == 0) {
				for (SubCommand c : commands) {
					if(sender.hasPermission(c.perm())){
					MessageManager.getInstance().info(p, "/lunarMobs " + c.name()+ " - " + c.info());
					}
				}
				return true;
			}
			
			SubCommand target = get(args[0]);
			
			if (target == null) {
				MessageManager.getInstance().severe(p, "/lunarMobs " + args[0] + " is not a valid subcommand!");
				return true;
			}
			
			ArrayList<String> a = new ArrayList<String>();
			a.addAll(Arrays.asList(args));
			a.remove(0);
			args = a.toArray(new String[a.size()]);
			
			try {
				target.onCommand(p, args);
			}
			
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return true;
	}
	
	private SubCommand get(String name) {
		for (SubCommand cmd : commands) {
			if (cmd.name().equalsIgnoreCase(name)) return cmd;
		}
		return null;
	}

}
