package src.codingdynasty.com.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import src.codingdynasty.com.Mobs;
import src.codingdynasty.com.PlayerLevel;
import src.codingdynasty.com.utils.MessageManager;
import src.codingdynasty.com.utils.RankType;

public class SetLevel extends SubCommand{
	
	public String perm = "LunarMobs.ADMIN";

	@Override
	public void onCommand(Player p, String[] args) {
		
		if(!p.hasPermission(perm)){
			p.sendMessage(ChatColor.RED + "You do not have access to that command!");
			return;
		}
		Player target = null;

		for(Player pl : Bukkit.getOnlinePlayers()){
			if(args[0].equalsIgnoreCase(pl.getName())){
				target = pl;
			}
		}
		
		if(target == null){
			MessageManager.getInstance().severe(p, "Player must be online! /lunarMobs <setLevel> <playername> <level>");
			return;
		}
		if(Mobs.playerLevel.containsKey(target.getUniqueId())){
			
			PlayerLevel level = Mobs.playerLevel.get(target.getUniqueId());
			
			try{
			Integer i = Integer.parseInt(args[1]);		
			level.setRank(i, RankType.CommandRank);
			
			MessageManager.getInstance().good(p, target.getName() + "'s level was set to " + Mobs.getLevel(target));
			MessageManager.getInstance().info(p, "You're level was set to " + Mobs.getLevel(target));

			}catch(Exception ex){
				MessageManager.getInstance().severe(p, "You must enter a number! /lunarMobs <setLevel> <playername> <level>");
			}
		}else{
			PlayerLevel level = new PlayerLevel(target);
			try{
				
				Integer i = Integer.parseInt(args[1]);
				level.setRank(i, RankType.CommandRank);
				
				MessageManager.getInstance().good(p, target.getName() + "'s level was set to " + Mobs.getLevel(target));
				MessageManager.getInstance().info(target, "You're level was set to " + Mobs.getLevel(target));
			}catch(NumberFormatException ex){
				System.out.println("Number not entered!");
				MessageManager.getInstance().severe(p, "You must enter a number! /lunarMobs <setLevel> <playername> <level>");
			}
		}
		
	}
	
	@Override
	public String perm(){
		return perm;
	}

	@Override
	public String name() {
		
		return "SetLevel";
	}

	@Override
	public String info() {
		
		return "Sets player's level";
	}

}
