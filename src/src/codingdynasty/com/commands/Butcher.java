package src.codingdynasty.com.commands;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import src.codingdynasty.com.utils.MessageManager;

public class Butcher extends SubCommand{
	
	public String perm = "LunarMobs.Admin";

	@Override
	public void onCommand(Player p, String[] args) {
		if(p.hasPermission(perm)){
			
			for(Entity e : Bukkit.getServer().getWorld(p.getWorld().getName()).getEntities()){
				
				if(!(e instanceof Player)){
					e.remove();
				}
				
			}
			MessageManager.getInstance().good(p, "All entities removed!");
			
		}
		
	}

	@Override
	public String name() {
		return "Butcher";
	}

	@Override
	public String info() {
		return "Butchers all mobs";
	}

	@Override
	public String perm() {
		return perm;
	}

	
	
}
