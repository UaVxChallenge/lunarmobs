package src.codingdynasty.com.commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import src.codingdynasty.com.utils.LocationManager;
import src.codingdynasty.com.utils.MessageManager;

public class inSpawn extends SubCommand{
	
	public String perm = "LunarMobs.ADMIN";

	@Override
	public void onCommand(Player p, String[] args) {
		if(!p.hasPermission(perm)){
			p.sendMessage(ChatColor.RED + "You do not have access to that command!");
			return;
		}
		if(LocationManager.isInSpawn(p)){
			MessageManager.getInstance().good(p, "You are in spawn!");
		}
		
	}
	
	@Override
	public String perm(){
		return perm;
	}

	@Override
	public String name() {
		return "inSpawn";
	}

	@Override
	public String info() {
		// TODO Auto-generated method stub
		return "Are you in spawn?";
	}

}
