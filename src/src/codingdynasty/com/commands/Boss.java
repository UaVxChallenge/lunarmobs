package src.codingdynasty.com.commands;

import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import src.codingdynasty.com.utils.BossType;
import src.codingdynasty.com.utils.MessageManager;

public class Boss extends SubCommand{
	
	public static HashMap<BossType, Location> BossSpawn = new HashMap<BossType, Location>();
	public static HashMap<BossType, Location> ArenaLoc1 = new HashMap<BossType, Location>();
	public static HashMap<BossType, Location> ArenaLoc2 = new HashMap<BossType, Location>();
	public static String perm = "LunarMobs.Admin";

	@Override
	public void onCommand(Player p, String[] args) {
		if(!p.hasPermission(perm)){
			return;
		}
		
		if(args.length != 2){
			MessageManager.getInstance().severe(p, "Correct usage: /LunarMobs Boss <setspawn | Arena[1,2]> <BossName>");
			return;
		}
		
		if(args[0].equalsIgnoreCase("setSpawn")){
			if(args[1].equalsIgnoreCase("PrinceSlime")){
				
				BossSpawn.put(BossType.Prince_Slime, p.getLocation());
				MessageManager.getInstance().good(p, "Prince Slime Spawn Set!");

			}else{
				
				//Add more bosses here
				
				
			}
		}else if(args[0].equalsIgnoreCase("Arena1")){
			
			if(args[1].equalsIgnoreCase("PrinceSlime")){
				
				ArenaLoc1.put(BossType.Prince_Slime, p.getLocation());
				MessageManager.getInstance().good(p, "Prince Slime Arena Point 1 Set!");
				
			}else{
				
				//Add mroe bosses here
				
			}
			
		}else if(args[0].equalsIgnoreCase("Arena2")){
			
			if(args[1].equalsIgnoreCase("PrinceSlime")){
				
				ArenaLoc2.put(BossType.Prince_Slime, p.getLocation());
				MessageManager.getInstance().good(p, "Prince Slime Arena Point 2 Set!");

			}else{
				
				//Add mroe bosses here
				
			}
			
		}
		
	}

	@Override
	public String name() {
		return "Boss";
	}

	@Override
	public String info() {
		return "Sets spawn information";
	}

	@Override
	public String perm() {
		return perm;
	}

}
