package src.codingdynasty.com.commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import src.codingdynasty.com.SLAPI;
import src.codingdynasty.com.utils.MessageManager;

public class setSpawn extends SubCommand{
	
	public static Location loc1;
	public static Location loc2;
	public String perm = "LunarMobs.ADMIN";

	@Override
	public void onCommand(Player p, String[] args) {
		if(!p.hasPermission("LunarMobs.ADMIN")){
			p.sendMessage(ChatColor.RED + "You do not have access to that command!");
			return;
		}
		if(args.length > 1){
			MessageManager.getInstance().severe(p, "Correct usage: /LunarMobs setSpawn <1 or 2>");
			return;
		}

		if(args[0].equalsIgnoreCase("1")){
			loc1 = p.getLocation();
			p.sendMessage(ChatColor.GREEN + "Postion 1 set");

		}else
		if(args[0].equalsIgnoreCase("2")){
			loc2 = p.getLocation();
			p.sendMessage(ChatColor.GREEN + "Postion 2 set");

			
		}else{
			return;
		}
		if(loc1 == null && loc2 == null){
			SLAPI.saveSpawnPoint(loc1, loc2);
		}

		
	}
	
	@Override
	public String perm(){
		return "LunarMobs.ADMIN";
	}
	
	
	@Override
	public String name() {
		
		return "SetSpawn";
	}

	@Override
	public String info() {
		return "Sets safe zone";
	}
	
	
}
