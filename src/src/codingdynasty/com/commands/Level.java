package src.codingdynasty.com.commands;

import org.bukkit.entity.Player;

import src.codingdynasty.com.Mobs;
import src.codingdynasty.com.PlayerLevel;
import src.codingdynasty.com.utils.MessageManager;

public class Level extends SubCommand{

	public String perm = "";
	
	@Override
	public void onCommand(Player p, String[] args) {

		if(Mobs.playerLevel.containsKey(p.getUniqueId())){
			PlayerLevel level = Mobs.playerLevel.get(p.getUniqueId());
			
			MessageManager.getInstance().good(p, "You are level " + level.getLevel());
			MessageManager.getInstance().info(p, level.getEXP() + "xp/" + level.getNeededXP() + " need for next level.");
		}
		
	}

	@Override
	public String name() {
		return "level";
	}

	@Override
	public String info() {
		return "Show your level stats";
	}

	@Override
	public String perm() {
		// TODO Auto-generated method stub
		return perm;
	}

}
