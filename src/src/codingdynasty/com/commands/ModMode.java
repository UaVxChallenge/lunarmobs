package src.codingdynasty.com.commands;

import java.util.ArrayList;

import org.bukkit.entity.Player;

import src.codingdynasty.com.utils.MessageManager;

public class ModMode extends SubCommand{
	
	public static String perm = "LunarMobs.Mod";
	public static ArrayList<Player> modMode = new ArrayList<Player>();

	@Override
	public void onCommand(Player p, String[] args) {
		
		if(args.length != 1){
			MessageManager.getInstance().severe(p, "Correct usage: /LunarMobs ModeMod <on|off>");
			return;
		}
		
		if(p.hasPermission(perm)){
			
			if(modMode.contains(p) && args[0].equalsIgnoreCase("off")){
				modMode.remove(p);	
				MessageManager.getInstance().severe(p, "Mod mode off");
			}
			if(!modMode.contains(p) && args[0].equalsIgnoreCase("on")){
				modMode.add(p);
				MessageManager.getInstance().good(p, "Mod mode on");
			}
			
			
		}
		
	}

	@Override
	public String name() {
		return "ModMode";
	}

	@Override
	public String info() {
		
		return "Stops Mob Spawning around you";
	}

	@Override
	public String perm() {
		return perm;
	}

	
	
	
}
