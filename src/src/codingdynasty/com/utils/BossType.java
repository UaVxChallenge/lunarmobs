package src.codingdynasty.com.utils;

public enum BossType {

	Prince_Slime;
	
	public static BossType[] getTypes(){
		return new BossType[]{Prince_Slime};
	}
	
}
