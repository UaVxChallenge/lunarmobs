package src.codingdynasty.com.utils;

import org.bukkit.entity.Player;

import src.codingdynasty.com.commands.Boss;
import src.codingdynasty.com.commands.setSpawn;
import src.codingdynasty.com.listener.SpawnSystem;

public class LocationManager {

	public static boolean isInSpawn(Player p) {
		if (setSpawn.loc1 == null || setSpawn.loc2 == null) {
			MessageManager.getInstance().severe(p, "One location must be null");
			return false;
		}

		int x1 = getMin((int) setSpawn.loc1.getX(), (int) setSpawn.loc2.getX());
		int x2 = getMax((int) setSpawn.loc1.getX(), (int) setSpawn.loc2.getX());
		int z1 = getMin((int) setSpawn.loc1.getZ(), (int) setSpawn.loc2.getZ());
		int z2 = getMax((int) setSpawn.loc1.getZ(), (int) setSpawn.loc2.getZ());
		for (int x = x1; x < x2; x++) {
			if ((int) p.getLocation().getX() == x) {
				for (int z = z1; z < z2; z++) {

					if ((int) p.getLocation().getZ() == z) {
						return true;
					}

				}

			}
		}

		return false;
	}

	public static boolean hasPlayerInBossZone(BossType type, Player p) {

		if (Boss.ArenaLoc1.get(type) == null
				|| Boss.ArenaLoc2.get(type) == null) {
			return false;
		}
		
		if(SpawnSystem.hasSpawn.get(type)){
			return false;
		}

		int x1 = getMin((int) Boss.ArenaLoc1.get(type).getX(),
				(int) Boss.ArenaLoc2.get(type).getX());
		int x2 = getMax((int) Boss.ArenaLoc1.get(type).getX(),
				(int) Boss.ArenaLoc2.get(type).getX());
		int z1 = getMin((int) Boss.ArenaLoc1.get(type).getZ(),
				(int) Boss.ArenaLoc2.get(type).getZ());
		int z2 = getMax((int) Boss.ArenaLoc1.get(type).getZ(),
				(int) Boss.ArenaLoc2.get(type).getZ());
			if (!SpawnSystem.hasSpawn.get(type)) {
				for (int x = x1; x < x2; x++) {
					if ((int) p.getLocation().getX() == x) {
						for (int z = z1; z < z2; z++) {

							if ((int) p.getLocation().getZ() == z) {
								SpawnSystem.hasSpawn.put(type, true);
								setBossRespawn(type);
								return true;
							}

						}

					}
				}
			}
		
		return false;

	}
	
	public static void setBossRespawn(BossType type){
		SpawnSystem.hasSpawn.put(type, false);
	}

	private static int getMax(int x1, int x2) {

		if (x1 > x2) {
			return x1;
		} else {
			return x2;
		}

	}

	private static int getMin(int x1, int x2) {

		if (x1 > x2) {
			return x2;
		} else {
			return x1;
		}

	}

}
