package src.codingdynasty.com.listener;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;

import src.codingdynasty.com.nms.CustomSkeleton;

public class SkeletonShootEvent implements Listener {

	@EventHandler
	public void onSkelyShoot(EntityShootBowEvent e) {

		net.minecraft.server.v1_8_R3.Entity en = ((CraftEntity) e.getEntity())
				.getHandle();

		if (en instanceof CustomSkeleton) {

			e.getProjectile().remove();

			Snowball s = (Snowball) e
					.getProjectile()
					.getWorld()
					.spawnEntity(e.getProjectile().getLocation(),
							EntityType.SNOWBALL);
			
			s.setShooter(e.getEntity());

			s.setVelocity(e.getProjectile().getVelocity());

		}

	}

	@EventHandler
	public void skelyHitEvent(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			if (e.getDamager() instanceof Snowball) {
				e.setCancelled(true);
				Snowball s = (Snowball) e.getDamager();
				net.minecraft.server.v1_8_R3.Entity en = ((CraftEntity) s.getShooter()).getHandle();

				if (en instanceof CustomSkeleton) {
					CustomSkeleton skely = (CustomSkeleton) en;
					p.damage((skely.getLevel() / 10) + 5.0);
					
				}
			}

		}
	}

}
