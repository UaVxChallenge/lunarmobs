package src.codingdynasty.com.listener;

import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import src.codingdynasty.com.Mobs;
import src.codingdynasty.com.PlayerLevel;
import src.codingdynasty.com.nms.CustomSkeleton;
import src.codingdynasty.com.nms.CustomZombie;
import src.codingdynasty.com.nms.PrinceSlime;
import src.codingdynasty.com.nms.SlimeGuard;

public class Kill implements Listener {

	Random r = new Random();

	@EventHandler
	public void onEntityDamage(EntityDamageEvent e) {
		net.minecraft.server.v1_8_R3.Entity en = ((CraftEntity) e.getEntity())
				.getHandle();

		if (en instanceof PrinceSlime) {
			PrinceSlime sBoss = (PrinceSlime) en;
			
			if(e.getCause().equals(DamageCause.ENTITY_ATTACK)){
				EntityDamageByEntityEvent ev = (EntityDamageByEntityEvent) e;
				
				if(ev.getDamager() instanceof Player){
					Player p = (Player) ev.getDamager();
					if(((PrinceSlime) en).getAssisters().contains(p)){
						((PrinceSlime) en).addPlayerAssist(p);
					}
				}
				
			}

			if (sBoss.getHealth() < (sBoss.getMaxHealth() * 0.75)
					&& sBoss.getPreviousHealth() > (sBoss.getMaxHealth() * 0.75)) {

				sBoss.spawnGuards(e.getEntity().getLocation());

			} else if (sBoss.getHealth() < (sBoss.getMaxHealth() * 0.50)
					&& sBoss.getPreviousHealth() > (sBoss.getMaxHealth() * 0.50)) {

				sBoss.spawnGuards(e.getEntity().getLocation());

			} else if (sBoss.getHealth() < (sBoss.getMaxHealth() * 0.25)
					&& sBoss.getPreviousHealth() > (sBoss.getMaxHealth() * 0.25)) {

				sBoss.spawnGuards(e.getEntity().getLocation());

			}

			sBoss.setPreviousHealth(sBoss.getHealth());
		}
	}

	@EventHandler
	public void onEntityDeath(EntityDeathEvent event) {

		event.setDroppedExp(0);
		event.getDrops().clear();
		if (r.nextInt(100) + 1 < 50) {
			for (int i = 0; i <= r.nextInt(5); i++) {
				event.getEntity()
						.getWorld()
						.dropItemNaturally(event.getEntity().getLocation(),
								new ItemStack(Material.GOLD_NUGGET));
			}
		}
		net.minecraft.server.v1_8_R3.Entity e = ((CraftEntity) event
				.getEntity()).getHandle();

		if (e instanceof CustomZombie) {
			CustomZombie zombie = (CustomZombie) e;

			if (event.getEntity().getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK) {
				Entity ev = ((EntityDamageByEntityEvent) event.getEntity()
						.getLastDamageCause()).getDamager();
				if (ev instanceof Player) {

					Player killer = (Player) ev;

					if (Mobs.playerLevel.containsKey(killer.getUniqueId())) {
						long xp = 0l;
						PlayerLevel level = Mobs.playerLevel.get(killer
								.getUniqueId());

						xp = ((zombie.getLevel() * 10) - 5);

						level.addXP(xp);
						killer.sendMessage(ChatColor.GREEN + "+" + xp + "xp");

					}
				}

			}

		} else if (e instanceof CustomSkeleton) {
			CustomSkeleton skeleton = (CustomSkeleton) e;

			if (event.getEntity().getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK) {
				Entity ev = ((EntityDamageByEntityEvent) event.getEntity()
						.getLastDamageCause()).getDamager();
				if (ev instanceof Player) {

					Player killer = (Player) ev;

					if (Mobs.playerLevel.containsKey(killer.getUniqueId())) {
						long xp = 0l;
						PlayerLevel level = Mobs.playerLevel.get(killer
								.getUniqueId());

						xp = ((skeleton.getLevel() * 10) + 5);

						level.addXP(xp);
						killer.sendMessage(ChatColor.GREEN + "+" + xp + "xp");

					}
				}

			}
		} else if (e instanceof PrinceSlime) {
			PrinceSlime prince = (PrinceSlime) e;

			if (event.getEntity().getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK) {
				Entity ev = ((EntityDamageByEntityEvent) event.getEntity()
						.getLastDamageCause()).getDamager();
				if (ev instanceof Player) {

					Player killer = (Player) ev;

					if (Mobs.playerLevel.containsKey(killer.getUniqueId())) {
						long xp = 0l;
						PlayerLevel level = Mobs.playerLevel.get(killer
								.getUniqueId());

						if(level.getLevel() > 60){
							xp = (prince.getLevel() * 15);
						}else if(level.getLevel() < 40){
							xp = (prince.getLevel() * 40);
						}else{
							xp = (prince.getLevel() * 30);
						}
						level.addXP(xp);
						killer.sendMessage(ChatColor.GREEN + "+" + xp + "xp");

					}
					
					for(Player pl : ((PrinceSlime) e).getAssisters()){
						if(pl.equals(killer)){
							
						}else{
							if(Mobs.playerLevel.containsKey(pl)){
								PlayerLevel playerLevel = Mobs.playerLevel.get(pl.getUniqueId());
								playerLevel.addXP(prince.getLevel() * 10);
								pl.sendMessage(ChatColor.GREEN + "+" + prince.getLevel() * 10 + "xp");
							}
						}
					}
				}

			}
		} else if (e instanceof SlimeGuard) {
			SlimeGuard guard = (SlimeGuard) e;

			if (event.getEntity().getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK) {
				Entity ev = ((EntityDamageByEntityEvent) event.getEntity()
						.getLastDamageCause()).getDamager();
				if (ev instanceof Player) {

					Player killer = (Player) ev;

					if (Mobs.playerLevel.containsKey(killer.getUniqueId())) {
						long xp = 0l;
						PlayerLevel level = Mobs.playerLevel.get(killer
								.getUniqueId());

						xp = ((guard.getLevel() * 15) + 5);

						level.addXP(xp);
						killer.sendMessage(ChatColor.GREEN + "+" + xp + "xp");

					}
				}

			}
		}

	}

	@EventHandler
	public void onEntitySpawn(EntitySpawnEvent e) {

		net.minecraft.server.v1_8_R3.Entity Crafte = ((CraftEntity) e
				.getEntity()).getHandle();

		if (e instanceof Player || Crafte instanceof CustomZombie) {

		} else if (Crafte instanceof CustomSkeleton) {

		} else if (Crafte instanceof PrinceSlime) {

		} else if (Crafte instanceof SlimeGuard) {

		} else {
			e.setCancelled(true);
		}

	}

	@EventHandler
	public void combustEvent(EntityCombustEvent e) {

		e.setCancelled(true);

	}

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		e.setDroppedExp(0);
		e.getDrops().clear();
		if (e.getEntity().getKiller() instanceof Player) {
			PlayerLevel playerLevel = Mobs.playerLevel.get(e.getEntity());
			PlayerLevel killerLevel = Mobs.playerLevel.get(e.getEntity()
					.getKiller());

			long xp = (playerLevel.getLevel() * 15) - 5;
			killerLevel.addXP(xp);
			e.getEntity().getKiller()
					.sendMessage(ChatColor.GREEN + "+" + xp + "xp");

		}
	}

}
