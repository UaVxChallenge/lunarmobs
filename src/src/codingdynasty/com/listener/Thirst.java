package src.codingdynasty.com.listener;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import src.codingdynasty.com.Mobs;

public class Thirst implements Listener{

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e){
		Random r = new Random();
		int i = r.nextInt(40);
		if(i == 5){
			if(e.getPlayer().getExp() == 0f){
				e.getPlayer().damage(1d);
			}else{
			e.getPlayer().setExp(e.getPlayer().getExp() - 0.05f);
			}
		}
		
	}
	
	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent e){
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Mobs.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				e.getPlayer().setExp(1.0f);
				
			}
		}, 2);
	}
	
	@EventHandler
    public void onPotionDrink(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (((event.getAction() == Action.RIGHT_CLICK_AIR) || (event.getAction() == Action.RIGHT_CLICK_BLOCK)) && (player.getItemInHand().getType() == Material.POTION)) {
            int hs = player.getInventory().getHeldItemSlot();
            drink(player,hs);
        }
    }
 
    public void drink(final Player player, int hs) {
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Mobs.getInstance(), new Runnable() {
            public void run() {
                if (player.getInventory().getItem(hs).getType() == Material.GLASS_BOTTLE) {
                    
                	player.setExp(player.getExp() + 0.1f);
                	if(player.getExp() > 1.0f){
                		player.setExp(1.0f);
                	}
                }
            }
        }, 35);
    }
}
