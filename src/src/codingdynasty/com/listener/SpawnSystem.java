package src.codingdynasty.com.listener;

import java.util.HashMap;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import src.codingdynasty.com.Mobs;
import src.codingdynasty.com.commands.Boss;
import src.codingdynasty.com.commands.ModMode;
import src.codingdynasty.com.utils.BossType;
import src.codingdynasty.com.utils.LocationManager;

public class SpawnSystem {

	private static Random r = new Random();
	public static HashMap<BossType, Boolean> hasSpawn = new HashMap<BossType, Boolean>();

	public static void checkPlayerChunk(Player p) {

		if (LocationManager.isInSpawn(p)) {
			return;
		}
		if (ModMode.modMode.contains(p)) {
			return;
		}
		if(!hasSpawn.containsKey(BossType.Prince_Slime)){
			hasSpawn.put(BossType.Prince_Slime, false);
		}
		if (hasSpawn.containsKey(BossType.Prince_Slime) && !hasSpawn.get(BossType.Prince_Slime)) {

			if (LocationManager.hasPlayerInBossZone(BossType.Prince_Slime, p)) {
				
				Mobs.spawnPrinceSlime(Boss.BossSpawn.get(BossType.Prince_Slime));
				return;
			}
		}
		for(BossType type : BossType.getTypes()){
		if(LocationManager.hasPlayerInBossZone(type, p)){
			return;
		}
		}
		spawnMobs(p);

	}

	public static void spawnTimer() {

		int i = r.nextInt(900 - 600) + 600;

		Bukkit.getServer().getScheduler()
				.scheduleSyncRepeatingTask(Mobs.getInstance(), new Runnable() {

					@Override
					public void run() {
						for (Player p : Bukkit.getOnlinePlayers()) {
							checkPlayerChunk(p);
						}

					}
				}, (long) i, (long) i);

	}

	public static void spawnMobs(Player p) {
		if (r.nextInt(10) > 5) {

			p.playSound(p.getLocation(), Sound.AMBIENCE_THUNDER, 0.8f, 0.8f);

			for (int i = r.nextInt(6); i < 6; i++) {

				int x = r.nextInt(16);
				x = Math.random() < 0.5 ? -x : x;
				int z = r.nextInt(16);
				z = Math.random() < 0.5 ? -z : z;

				Location location = new Location(p.getWorld(), p.getLocation()
						.getX() + x, p.getLocation().getY() + 3, p
						.getLocation().getZ() + z);

				if (p.getLocation().getChunk().getEntities().length > 20) {
					return;
				}

				int chance = r.nextInt(10);
				if (chance < 6) {
					Mobs.spawnCustomZombie(location, Mobs.getLevel(p));
				} else {
					Mobs.spawnCustomSkeleton(location, Mobs.getLevel(p));

				}

			}

		}

	}

}
