package src.codingdynasty.com;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import net.minecraft.server.v1_8_R3.ItemStack;
import net.minecraft.server.v1_8_R3.Items;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import src.codingdynasty.com.commands.Boss;
import src.codingdynasty.com.commands.CommandManager;
import src.codingdynasty.com.commands.setSpawn;
import src.codingdynasty.com.listener.Kill;
import src.codingdynasty.com.listener.SkeletonShootEvent;
import src.codingdynasty.com.listener.SpawnSystem;
import src.codingdynasty.com.listener.Thirst;
import src.codingdynasty.com.nms.CustomEntityType;
import src.codingdynasty.com.nms.CustomSkeleton;
import src.codingdynasty.com.nms.CustomZombie;
import src.codingdynasty.com.nms.PrinceSlime;
import src.codingdynasty.com.nms.SlimeGuard;
import src.codingdynasty.com.utils.BossType;

public class Mobs extends JavaPlugin implements Listener{
	
	public static List<Chunk> chunkList = new ArrayList<Chunk>();
	public static Map<Chunk, Long> chunkSpawnTimer = new HashMap<Chunk, Long>();
	public static Plugin plugin = null;
	public static HashMap<UUID, PlayerLevel> playerLevel = new HashMap<UUID, PlayerLevel>(); 

	public void onEnable(){
		
		plugin = this;
		
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		Bukkit.getServer().getPluginManager().registerEvents(new Kill(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new SkeletonShootEvent(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new Thirst(), this);
		CommandManager cm = new CommandManager();
		cm.setup();
		getCommand("lunarMobs").setExecutor(cm);

		SLAPI.createConfig();
		SLAPI.loadSpawnPoint();
		for(BossType type : BossType.getTypes()){
		SLAPI.loadBossSpawnPoint(type);
		SLAPI.loadBossArena(type);
		}
		
		for(Player p : Bukkit.getOnlinePlayers()){
			
				PlayerLevel level = new PlayerLevel(p);
				playerLevel.put(p.getUniqueId(), level);
				level.setExp(SLAPI.loadExp(p.getUniqueId()));
			
		}
		
		
	    Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable()
	    {
	      public void run()
	      {
	    	 for(Player p : Bukkit.getOnlinePlayers()){
	    		 if(playerLevel.containsKey(p.getUniqueId())){
	    			 PlayerLevel level = playerLevel.get(p.getUniqueId());
	    			 level.updateScoreboard();
	    		 }else{
	    		 }


	    	 }
	      }
	    }
	    , 60l, 60l);
			
	    SpawnSystem.spawnTimer();
	    
	}
	
	public void onDisable(){
		
		if(setSpawn.loc1 != null && setSpawn.loc2 != null){
			SLAPI.saveSpawnPoint(setSpawn.loc1, setSpawn.loc2);
		}
			for(BossType type : BossType.getTypes()){
				if(Boss.ArenaLoc1.containsKey(type) && Boss.ArenaLoc2.containsKey(type)){
				SLAPI.saveBossArena(type, Boss.ArenaLoc1.get(type), Boss.ArenaLoc2.get(type));
				SLAPI.saveBossSpawnPoint(Boss.BossSpawn.get(type), type);
				}
			}
		
		for(UUID id : playerLevel.keySet()){
			PlayerLevel pl = playerLevel.get(id);
			try {
				SLAPI.saveLevel(id, pl.getLevel());
				SLAPI.saveExp(id, pl.getEXP());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static Plugin getInstance(){
		
		return plugin;
		
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e){
		PlayerLevel level;
		if(!playerLevel.containsKey(e.getPlayer().getUniqueId())){
			level = new PlayerLevel(e.getPlayer());
			playerLevel.put(e.getPlayer().getUniqueId(), level);
			level.updateScoreboard();
		}else{
			level = playerLevel.get(e.getPlayer().getUniqueId());
			level.updateScoreboard();
		}
	
	}
	
	public static int getLevel(Player p){
		
		if(playerLevel.containsKey(p.getUniqueId())){
			PlayerLevel Plevel = playerLevel.get(p.getUniqueId());
			int level = Plevel.getLevel();
			return level;
			
		}else{
			return 0;
		}
		
	}
	
	public static void spawnCustomZombie(Location l, int Level){
		CraftWorld cWorld = ((CraftWorld) l.getWorld());
		net.minecraft.server.v1_8_R3.World world = cWorld.getHandle();
		Level = (Level + 1);
		CustomZombie zombie = new CustomZombie(world);
		zombie.setLevel(Level);
		((CustomZombie)zombie).setEquipment(4, new ItemStack(Items.GOLDEN_HELMET));
		zombie.setCustomName(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Bandit " + ChatColor.YELLOW + "lvl " + Level);
		CustomEntityType.spawnCustomZombie(zombie, l);
	}
	public static void spawnCustomSkeleton(Location l, int Level){
		CraftWorld cWorld = ((CraftWorld) l.getWorld());
		net.minecraft.server.v1_8_R3.World world = cWorld.getHandle();
		CustomSkeleton skely = new CustomSkeleton(world);
		Level = Level + 1;
		skely.setLevel(Level);
		skely.setCustomName(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Bandit " + ChatColor.YELLOW + "lvl " + Level);
		skely.setSkeletonType(0);
		((CustomSkeleton) skely).setEquipment(4, new ItemStack(Items.GOLDEN_HELMET));
		((CustomSkeleton) skely).setEquipment(0, new ItemStack(Items.BOW));
		CustomEntityType.spawnCustomSkeleton(skely, l);
	}
	public static void spawnPrinceSlime(Location l){
		CraftWorld cWorld = ((CraftWorld) l.getWorld());
		net.minecraft.server.v1_8_R3.World world = cWorld.getHandle();
		PrinceSlime king = new PrinceSlime(world);
		king.setSize(10);
		king.setHealth();
		king.setCustomName(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Prince Slime " + ChatColor.YELLOW + "lvl 50");
		CustomEntityType.spawnPrinceSlime(king, l);

		
	}
	
	public static void spawnGuardSlimes(Location loc){
		Random r = new Random();
		
			int x = r.nextInt(16);
			x = Math.random() < 0.5 ? -x : x;
			int z = r.nextInt(16);
			z = Math.random() < 0.5 ? -z : z;
			
			loc = new Location(loc.getWorld(), loc.getX() + x, loc.getY() + 3, loc.getZ() + z);

		CraftWorld cWorld = ((CraftWorld) loc.getWorld());
		net.minecraft.server.v1_8_R3.World world = cWorld.getHandle();
		SlimeGuard king = new SlimeGuard(world);
		king.setSize(5);
		king.setHealth();
		king.setCustomName(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Guard Slime " + ChatColor.YELLOW + "lvl 25");
		CustomEntityType.spawnGuardSlime(king, loc);

		
	}
}