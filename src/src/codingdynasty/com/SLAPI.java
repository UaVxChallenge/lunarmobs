package src.codingdynasty.com;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import src.codingdynasty.com.commands.Boss;
import src.codingdynasty.com.commands.setSpawn;
import src.codingdynasty.com.utils.BossType;

public class SLAPI {
	public static File file = new File(Mobs.getInstance().getDataFolder(),
			"levels.yml");
	public static YamlConfiguration levelsYAML = new YamlConfiguration();
	public static File spawnsFile = new File(
			Mobs.getInstance().getDataFolder(), "spawns.yml");
	public static YamlConfiguration spawnsYAML = new YamlConfiguration();

	public static void createConfig() {
		try {
			if (!Mobs.getInstance().getDataFolder().exists()) {
				Mobs.getInstance().getDataFolder().mkdirs();
			}
			if (!file.exists()) {
				Mobs.getInstance().getLogger()
						.info("levels.yml not found, creating!");
				file.createNewFile();
				levelsYAML.load(file);
			} else {
				levelsYAML.load(file);
				Mobs.getInstance().getLogger()
						.info("levels.yml found, loading!");
			}
			if (!spawnsFile.exists()) {
				Mobs.getInstance().getLogger()
						.info("spawns.yml not found, creating!");
				spawnsFile.createNewFile();
				spawnsYAML.load(spawnsFile);
			} else {
				spawnsYAML.load(spawnsFile);
				Mobs.getInstance().getLogger()
						.info("spawns.yml found, loading!");
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	public static boolean containsPlayer(UUID id) {

		if (levelsYAML.contains("" + id)) {
			return true;
		}

		return false;
	}

	public static int loadLevel(UUID id) {

		int i = levelsYAML.getInt(id + ".level");

		return i;

	}

	public static void saveLevel(Player p, int level) throws IOException {
		levelsYAML.set("" + p.getUniqueId() + ".level", level);
		levelsYAML.save(file);
	}

	public static void saveLevel(UUID id, int level) throws IOException {
		levelsYAML.set("" + id + ".level", level);
		levelsYAML.save(file);
	}

	public static long loadExp(UUID id) {

		long i = levelsYAML.getLong(id + ".exp");

		return i;

	}

	public static void saveExp(Player p, long xp) throws IOException {
		levelsYAML.set(p.getUniqueId() + ".exp", xp);
		levelsYAML.save(file);
	}

	public static void saveExp(UUID id,long xp) throws IOException {
		levelsYAML.set(id + ".exp", xp);
		levelsYAML.save(file);
	}

	public static void saveSpawnPoint(Location loc1, Location loc2) {

		spawnsYAML.set("Spawn.1.x", loc1.getX());
		spawnsYAML.set("Spawn.1.z", loc1.getZ());
		spawnsYAML.set("Spawn.1.world", loc1.getWorld().getName());
		spawnsYAML.set("Spawn.2.x", loc2.getX());
		spawnsYAML.set("Spawn.2.z", loc2.getZ());
		spawnsYAML.set("Spawn.2.world", loc2.getWorld().getName());

		try {
			spawnsYAML.save(spawnsFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void loadSpawnPoint() {
		if (!spawnsYAML.contains("Spawn.1") || !spawnsYAML.contains("Spawn.2")) {
			Location loc1 = new Location(Bukkit.getServer().getWorld("World"),
					5.0, 5.0, 5.0);
			Location loc2 = new Location(Bukkit.getServer().getWorld("World"),
					5.0, 5.0, 5.0);
			setSpawn.loc1 = loc1;
			setSpawn.loc2 = loc2;
		} else {
			Location loc1 = new Location(Bukkit.getServer().getWorld(
					spawnsYAML.getString("Spawn.1.world")),
					spawnsYAML.getDouble("Spawn.1.x"), 5.0,
					spawnsYAML.getDouble("Spawn.1.z"));
			Location loc2 = new Location(Bukkit.getServer().getWorld(
					spawnsYAML.getString("Spawn.2.world")),
					spawnsYAML.getDouble("Spawn.2.x"), 5.0,
					spawnsYAML.getDouble("Spawn.2.z"));
			setSpawn.loc1 = loc1;
			setSpawn.loc2 = loc2;
		}
	}

	public static void saveBossSpawnPoint(Location loc1,
			BossType type) {

		spawnsYAML.set(type.toString() + ".Spawn.1.x", loc1.getX());
		spawnsYAML.set(type.toString() + ".Spawn.1.y", loc1.getY());
		spawnsYAML.set(type.toString() + ".Spawn.1.z", loc1.getZ());
		spawnsYAML.set(type.toString() + ".Spawn.1.world", loc1.getWorld()
				.getName());

		try {
			spawnsYAML.save(spawnsFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void loadBossSpawnPoint(BossType type) {
		if (spawnsYAML.contains(type.toString() + ".Spawn.1")) {
			Location loc1 = new Location(Bukkit.getServer().getWorld(
					spawnsYAML.getString(type.toString() + ".Spawn.1.world")),
					spawnsYAML.getDouble(type.toString() + ".Spawn.1.x"), spawnsYAML.getDouble(type.toString() + ".Spawn.1.y"),
					spawnsYAML.getDouble(type.toString() + ".Spawn.1.z"));
			Boss.BossSpawn.put(type, loc1);
			System.out.println(ChatColor.GREEN + "Boss Spawn loaded!");
		}
	}

	public static void saveBossArena(BossType type, Location loc1, Location loc2) {

		spawnsYAML.set(type.toString() + ".Arena.1.x", loc1.getX());
		spawnsYAML.set(type.toString() + ".Arena.1.z", loc1.getZ());
		spawnsYAML.set(type.toString() + ".Arena.1.world", loc1.getWorld()
				.getName());
		spawnsYAML.set(type.toString() + ".Arena.2.x", loc2.getX());
		spawnsYAML.set(type.toString() + ".Arena.2.z", loc2.getZ());
		spawnsYAML.set(type.toString() + ".Arena.2.world", loc2.getWorld()
				.getName());

		try {
			spawnsYAML.save(spawnsFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void loadBossArena(BossType type) {
		if(spawnsYAML.contains(type.toString())){
			Location loc1 = new Location(Bukkit.getServer().getWorld(
					spawnsYAML.getString(type.toString() + ".Arena.1.world")),
					spawnsYAML.getDouble(type.toString() + ".Arena.1.x"), 5.0,
					spawnsYAML.getDouble(type.toString() + ".Arena.1.z"));
			Location loc2 = new Location(Bukkit.getServer().getWorld(
					spawnsYAML.getString(type.toString() + ".Arena.2.world")),
					spawnsYAML.getDouble(type.toString() + ".Arena.2.x"), 5.0,
					spawnsYAML.getDouble(type.toString() + ".Arena.2.z"));
			Boss.ArenaLoc1.put(type, loc1);
			Boss.ArenaLoc2.put(type, loc2);
			System.out.println(ChatColor.GREEN + "Boss Arena's loaded!");
		}
	}

}
