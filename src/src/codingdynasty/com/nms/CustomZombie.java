package src.codingdynasty.com.nms;

import java.lang.reflect.Field;

import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityIronGolem;
import net.minecraft.server.v1_8_R3.EntityPigZombie;
import net.minecraft.server.v1_8_R3.EntityVillager;
import net.minecraft.server.v1_8_R3.EntityZombie;
import net.minecraft.server.v1_8_R3.GenericAttributes;
import net.minecraft.server.v1_8_R3.PathfinderGoalHurtByTarget;
import net.minecraft.server.v1_8_R3.PathfinderGoalMeleeAttack;
import net.minecraft.server.v1_8_R3.PathfinderGoalMoveThroughVillage;
import net.minecraft.server.v1_8_R3.PathfinderGoalNearestAttackableTarget;
import net.minecraft.server.v1_8_R3.World;

public class CustomZombie extends EntityZombie{
	
	public CustomZombie(World world) {
		super(world);	
	}
	
	private int level = 0;
	
		public void setLevel(int Level){
			double d = (double) Level + 2;
			level = Level;
		    getAttributeInstance(GenericAttributes.ATTACK_DAMAGE).setValue(d);
		}
		
		public int getLevel(){
			return level;
		}
		
		@SuppressWarnings({ "rawtypes", "unchecked" })
		protected void n(){
		    this.goalSelector.a(4, new PathfinderGoalMeleeAttack(this, EntityVillager.class, 1.0D, true));
		    this.goalSelector.a(4, new PathfinderGoalMeleeAttack(this, EntityIronGolem.class, 1.0D, true));
		    this.goalSelector.a(6, new PathfinderGoalMoveThroughVillage(this, 1.0D, false));
		    this.targetSelector.a(1, new PathfinderGoalHurtByTarget(this, true, new Class[] { EntityPigZombie.class }));
		    this.targetSelector.a(2, new PathfinderGoalNearestAttackableTarget(this, EntityHuman.class, true));
		    this.targetSelector.a(2, new PathfinderGoalNearestAttackableTarget(this, EntityIronGolem.class, true));
		}
	  
	   public static Object getPrivateField(String fieldName, Class<?> clazz, Object object)
	    {
	        Field field;
	        Object o = null;
	        try
	        {
	            field = clazz.getDeclaredField(fieldName);
	            field.setAccessible(true);
	            o = field.get(object);
	        }
	        catch(NoSuchFieldException e)
	        {
	            e.printStackTrace();
	        }
	        catch(IllegalAccessException e)
	        {
	            e.printStackTrace();
	        }
	        return o;
	    }
	  
}
