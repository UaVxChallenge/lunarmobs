package src.codingdynasty.com.nms;

import java.util.ArrayList;
import java.util.Random;

import net.minecraft.server.v1_8_R3.EntitySlime;
import net.minecraft.server.v1_8_R3.GenericAttributes;
import net.minecraft.server.v1_8_R3.World;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import src.codingdynasty.com.Mobs;

public class PrinceSlime extends EntitySlime {
	
	Random r = new Random();
	public float previousHealth;
	public ArrayList<Player> assist = new ArrayList<Player>();
	
	public PrinceSlime(World world) {
		super(world);
	}
	
	public int getLevel(){
		return 50;
	}

	public void setHealth() {
		getAttributeInstance(GenericAttributes.maxHealth).setValue(500.0D);
		setHealth(getMaxHealth());
		previousHealth = getMaxHealth();
	}
	
	public float getPreviousHealth(){
		return previousHealth;
	}
	
	public void setPreviousHealth(float f){
		previousHealth = f;
	}
	
	public void spawnGuards(Location loc){
		
		for (int i = 0; i < 6; i++) {

			Mobs.spawnGuardSlimes(loc);
			
		}
		
	}
	
	public void addPlayerAssist(Player p){
		if(assist.contains(p)){
			return;
		}else{
			assist.add(p);
		}
	}
	
	public ArrayList<Player> getAssisters(){
		return assist;
	}

}
