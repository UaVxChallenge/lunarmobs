package src.codingdynasty.com.nms;

import net.minecraft.server.v1_8_R3.DifficultyDamageScaler;
import net.minecraft.server.v1_8_R3.Enchantment;
import net.minecraft.server.v1_8_R3.EnchantmentManager;
import net.minecraft.server.v1_8_R3.EntityArrow;
import net.minecraft.server.v1_8_R3.EntityLiving;
import net.minecraft.server.v1_8_R3.EntitySkeleton;
import net.minecraft.server.v1_8_R3.IRangedEntity;
import net.minecraft.server.v1_8_R3.ItemStack;
import net.minecraft.server.v1_8_R3.Items;
import net.minecraft.server.v1_8_R3.World;

import org.bukkit.craftbukkit.v1_8_R3.event.CraftEventFactory;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityShootBowEvent;

public class CustomSkeleton extends EntitySkeleton implements IRangedEntity{

	public CustomSkeleton(World world) {
		super(world);
	}
	
	private int level = 0;
	
	public void setLevel(int Level){
		level = Level;
	}
	
	public int getLevel(){
		return level;
	}

	  protected void a(DifficultyDamageScaler difficultydamagescaler)
	  {
	    super.a(difficultydamagescaler);
	    setEquipment(0, new ItemStack(Items.BOW));
	  }
	  
		public void a(EntityLiving entityliving, float f) {
			EntityArrow entityarrow = new EntityArrow(this.world, this,
					entityliving, 1.6F, 14 - this.world.getDifficulty().a() * 4);
			int i = EnchantmentManager.getEnchantmentLevel(
					Enchantment.ARROW_DAMAGE.id, bA());
			int j = EnchantmentManager.getEnchantmentLevel(
					Enchantment.ARROW_KNOCKBACK.id, bA());

			entityarrow.b(f * 2.0F + this.random.nextGaussian() * 0.25D
					+ this.world.getDifficulty().a() * 0.11F);
			if (i > 0) {
				entityarrow.b(entityarrow.j() + i * 0.5D + 0.5D);
			}

			if (j > 0) {
				entityarrow.setKnockbackStrength(j);
			}

			if ((EnchantmentManager.getEnchantmentLevel(Enchantment.ARROW_FIRE.id,
					bA()) > 0) || (getSkeletonType() == 1)) {
				EntityCombustEvent event = new EntityCombustEvent(
						entityarrow.getBukkitEntity(), 100);
				this.world.getServer().getPluginManager().callEvent(event);

				if (!event.isCancelled()) {
					entityarrow.setOnFire(event.getDuration());
				}

			}

			EntityShootBowEvent event = CraftEventFactory.callEntityShootBowEvent(
					this, bA(), entityarrow, 1.8F);
			if (event.isCancelled()) {
				event.getProjectile().remove();
				return;
			}

			if (event.getProjectile() == entityarrow.getBukkitEntity()) {
				this.world.addEntity(entityarrow);
			}

			makeSound("random.bow", 1.0F, 1.0F / (bc().nextFloat() * 0.4F + 0.8F));
		}

	  
	  public void setEquipment(int i, ItemStack itemstack) {
		    super.setEquipment(i, itemstack);
		    if ((!this.world.isClientSide) && (i == 0))
		      n();
		  }
}
