package src.codingdynasty.com.nms;

import java.lang.reflect.Field;
import java.util.Map;

import net.minecraft.server.v1_8_R3.Entity;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

public enum CustomEntityType {
	
    CUSTOM_ZOMBIE("Zombie", 54, CustomZombie.class),CUSTOM_SKELETON("Skeleton", 51, CustomSkeleton.class),SLIME_PRINCE("PrinceSlime", 55, PrinceSlime.class),SLIME_GUARD("SlimeGuard", 55, SlimeGuard.class);
    
    private CustomEntityType(String name, int id, Class<? extends Entity> custom) {
        addToMaps(custom, name, id);
    }
   
    
    public static CustomZombie spawnCustomZombie(CustomZombie entity, Location loc) {
        entity.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
        ((CraftWorld)loc.getWorld()).getHandle().addEntity(entity, SpawnReason.NATURAL);
        return entity;
    }
    
    public static CustomSkeleton spawnCustomSkeleton(CustomSkeleton entity, Location loc) {
        entity.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
        ((CraftWorld)loc.getWorld()).getHandle().addEntity(entity, SpawnReason.NATURAL);
        return entity;
    }
    
    public static PrinceSlime spawnPrinceSlime(PrinceSlime entity, Location loc) {
        entity.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
        ((CraftWorld)loc.getWorld()).getHandle().addEntity(entity, SpawnReason.NATURAL);
        return entity;
    }
   
    public static SlimeGuard spawnGuardSlime(SlimeGuard entity, Location loc) {
        entity.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
        ((CraftWorld)loc.getWorld()).getHandle().addEntity(entity, SpawnReason.NATURAL);        
        return entity;
    }
    
    @SuppressWarnings("unchecked")
	private static void addToMaps(Class<?> clazz, String name, int id) {
        ((Map<String, Class<?>>)getPrivateField("c", net.minecraft.server.v1_8_R3.EntityTypes.class, null)).put(name, clazz);
        ((Map<Class<?>, String>)getPrivateField("d", net.minecraft.server.v1_8_R3.EntityTypes.class, null)).put(clazz, name);
        ((Map<Class<?>, Integer>)getPrivateField("f", net.minecraft.server.v1_8_R3.EntityTypes.class, null)).put(clazz, Integer.valueOf(id));
    }
	
    public static Object getPrivateField(String fieldName, Class<?> clazz, Object object) {
        Field field;
        Object o = null;

        try
        {
            field = clazz.getDeclaredField(fieldName);

            field.setAccessible(true);

            o = field.get(object);
        }
        catch(NoSuchFieldException e)
        {
            e.printStackTrace();
        }
        catch(IllegalAccessException e)
        {
            e.printStackTrace();
        }

        return o;
    }
    
}
