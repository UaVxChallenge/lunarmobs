package src.codingdynasty.com.nms;

import net.minecraft.server.v1_8_R3.EntitySlime;
import net.minecraft.server.v1_8_R3.GenericAttributes;
import net.minecraft.server.v1_8_R3.World;

public class SlimeGuard extends EntitySlime{

	public SlimeGuard(World world) {
		super(world);
	}
	
	public int Level = 25;
	
	public void setHealth() {
		getAttributeInstance(GenericAttributes.maxHealth).setValue(50.0D);
		setHealth(getMaxHealth());
	}
	
	public int getLevel(){
		return Level;
	}

}
