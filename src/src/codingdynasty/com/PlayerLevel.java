package src.codingdynasty.com;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import src.codingdynasty.com.utils.MessageManager;
import src.codingdynasty.com.utils.RankType;

public class PlayerLevel {

	private int level;
	private Player player;
	private long exp = 0l;
	private long neededXP = 0l;
    private Scoreboard sb = null;
    private Objective ob = null;
    private Scoreboard head = null;
    private Objective headOB = null;

	public PlayerLevel(Player player) {

		this.player = player;

		if (!Mobs.getInstance().getDataFolder().exists()
				&& !SLAPI.file.exists()) {
			SLAPI.createConfig();
		}

		if (SLAPI.containsPlayer(player.getUniqueId())) {
			level = SLAPI.loadLevel(player.getUniqueId());
			neededXP = (100 * (level ^ 2));
			exp = SLAPI.loadExp(player.getUniqueId());
		} else {
			level = 1;
			try {
				SLAPI.saveLevel(player, 1);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		setDefaultRank(level);

	}

	public void addXP(long xp) {

		exp = exp + xp;

		if (exp >= neededXP) {
			levelUp();
		}

	}

	public void setDefaultRank(int lvl) {

		setRank(level, RankType.Default);

	}

	public void levelUp() {

		setRank(level, RankType.LevelUP);

	}
	
	private void checkIfDoubleLevel(){
		
		if(this.exp > this.neededXP){
			levelUp();
		}
		
	}

	public void setRank(int lvl, RankType type) {
		if (type == RankType.LevelUP) {
			lvl = lvl + 1;
			this.level = lvl;
			long xp = this.exp;
			long need = this.neededXP;
			MessageManager.getInstance().info(
					player,
					"You have leveled up to " + ChatColor.GREEN + (this.level)
							+ ChatColor.YELLOW + "!");
			this.exp = (xp - need);
			this.neededXP = (long) (100 * (Math.pow((double)level, 2)));
			checkIfDoubleLevel();
		}else{
		this.level = lvl;
		this.exp = 0;
		this.neededXP = (long) (100 * (Math.pow((double)level, 2)));
		}
		if (this.level >= 40) {
			if (!player.hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE)) {
				player.addPotionEffect(new PotionEffect(
						PotionEffectType.DAMAGE_RESISTANCE, 100000000, Math
								.round(this.level / 40) - 1));
			}
			if (this.level % 40 == 0) {
				player.addPotionEffect(new PotionEffect(
						PotionEffectType.DAMAGE_RESISTANCE, 100000000,
						(this.level / 40) - 1));
				MessageManager.getInstance().info(
						player,
						"You have earned Resistance " + ChatColor.GREEN
								+ (this.level / 40) + ChatColor.YELLOW + "!");
			}
		}else{
			if(player.hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE)){
				player.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
			}
		}

		if (this.level >= 10) {
			if (player.getMaxHealth() == (20.0 + (Math.round((this.level / 10)) + 1))) {
				player.setMaxHealth(20.0 + (Math.round(this.level / 10) + 1));
			}
			if (this.level % 10 == 0) {
				player.setMaxHealth(20.0 + ((this.level / 10) + 1));
				MessageManager.getInstance().info(
						player,
						"Your now have " + ChatColor.GREEN
								+ (((this.level / 10) + 21) / 2)
								+ ChatColor.YELLOW + " hearts!");
			}
		} else {
			if (player.getMaxHealth() != 20.0) {
				player.setMaxHealth(20.0);
			}
		}
		headScoreboard();
	}

	public int getLevel() {
		return level;
	}
	
	public void setExp(long l){
		this.exp = l;
	}
	
	public long getEXP(){
		return exp;
	}
	
	public long getNeededXP(){
		return neededXP;
	}


	  public void updateScoreboard()
	  {
		if(sb == null){
			  sb = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
			    ob = player.getScoreboard().getObjective(DisplaySlot.SIDEBAR);
		      ob = sb.registerNewObjective("vault", "dummy");
		}
		if(sb != null){
			ob.unregister();
		  ob = sb.registerNewObjective("vault", "dummy");
	      ob.setDisplayName(ChatColor.DARK_RED + "The Vault");
	      ob.setDisplaySlot(DisplaySlot.SIDEBAR);
	      ob.getScore(" ").setScore(12);
	      ob.getScore(ChatColor.DARK_RED + "Credits:").setScore(11);
	      ob.getScore(ChatColor.RED + "" + getPlayerNuggets(player)).setScore(10);
	      ob.getScore("  ").setScore(9);
	      ob.getScore(ChatColor.DARK_BLUE + "Thirst: ").setScore(8);
	      ob.getScore(ChatColor.AQUA + "" + (int)(player.getExp() * 100.0F) + "%").setScore(7);
	      ob.getScore("    ").setScore(6);
	      ob.getScore(ChatColor.DARK_GREEN + "Next Level: ").setScore(5);
	      ob.getScore(ChatColor.GREEN + "" + (this.neededXP - this.exp)).setScore(4);
	      ob.getScore("   ").setScore(3);
	      ob.getScore(ChatColor.DARK_PURPLE + "Players: ").setScore(2);
	      ob.getScore(ChatColor.LIGHT_PURPLE + "" + Bukkit.getOnlinePlayers().size()).setScore(1);
	      
	      player.setScoreboard(sb);
		}
	  }

	  private int getPlayerNuggets(Player p)
	  {
	    int c = 0;
	    for (ItemStack is : p.getInventory().getContents())
	    {
	      if (is != null)
	      {
	        if (is.getType().equals(Material.GOLD_NUGGET))
	          c += is.getAmount(); 
	      }
	    }
	    return c;
	  }
	  
	  private void headScoreboard(){
		  
		  if(head == null){
		
		  head = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
		  	headOB = player.getScoreboard().getObjective(DisplaySlot.BELOW_NAME);
		  	headOB = head.registerNewObjective("level", "level");
		  }
		  if(head != null){
			  headOB.unregister();
			  headOB = head.registerNewObjective("level", "level");
			  
			  headOB.setDisplayName(ChatColor.YELLOW + "Lvl " + this.level);
			  player.setScoreboard(head);
			  
		  }
	  }

}
